//
//  SearchRequest.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import Foundation

class UserDetailsRequestOperation: RequestOperationProtocol {
    
    typealias ResponseModel = UserDetailsRequestResponseModel

    struct UserDetailsRequestResponseModel: ServerResponseModelProtocol {

        typealias Output = [UserDetails]

        enum CodingKeys: String, CodingKey {
            case data = "items"
        }

        var data: [UserDetails]

    }
    
    let inputParameters: [String : String]? = nil
    let urlParameter: String? = nil
    
    var request: RequestProtocol {
        return UserDetailsRequest(userLogin: userLogin)
    }
    
    private let userLogin: String
    
    init(userLogin: String) {
        self.userLogin = userLogin
    }
    
}

class UserDetailsRequest: RequestProtocol {
    required init(query parameters: [String : String]?, urlParameter urlParam: String?) {
        self.externalParameters = parameters
        self.urlParameter = urlParam
        self.userLogin = urlParam ?? ""
    }
    
    let urlParameter: String?
    let externalParameters: [String : String]?
    let staticParameters: [String : String]? = nil
    let parameters: [String : String]? = nil
    
    let dataType: DataType = .JSON
    let method: HTTPMethodType = .get
    
    var path: String {
        return "users/\(userLogin)"
    }
    
    private var userLogin: String
    private var params: [String : String]?
    
    init(userLogin: String) {
        self.userLogin = userLogin
        self.urlParameter = userLogin
        self.externalParameters = nil
    }

}
