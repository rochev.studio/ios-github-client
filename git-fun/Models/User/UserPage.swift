//
//  UserPage.swift
//  git-fun
//
//  Created by Paul Vasilenko on 29.09.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

protocol UserPageProtocol {

    var pageNumber: Int? { get set }
    var totalCount: Int? { get }
    var isLoaded: Bool { get }
    var items: [User]? { get }
    var lastPage: Int? { get }
    
}

struct UserPage: UserPageProtocol, Codable {

    var totalCount: Int?
    var isLoaded: Bool = false
    var items: [User]?
    var pageNumber: Int?
    var lastPage: Int?
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
        case totalCount = "total_count"
    }
    
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: UserPage.CodingKeys.self)
        items = try map.decode([User].self, forKey: .items)
        isLoaded = !(items?.isEmpty ?? true)
        totalCount = try map.decodeIfPresent(Int.self, forKey: .totalCount)
        let last = (totalCount ?? 0)/20 + ((totalCount ?? 0)%20 > 0 ? 1 : 0)
        lastPage = (last > 50) ? 50 : last
    }
}
