//
//  UIView+Extension.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import UIKit

extension UIView {
    
    func setIsHidden(_ hidden: Bool, animated: Bool = true) {
        if animated {
            if isHidden && !hidden {
                alpha = 0.0
                isHidden = false
            }
            UIView.animate(withDuration: 0.25) { [weak self] in
                self?.alpha = hidden ? 0.0 : 1.0
            } completion: { [weak self] _ in
                self?.isHidden = hidden
            }
        } else {
            isHidden = hidden
        }
    }
    
}
