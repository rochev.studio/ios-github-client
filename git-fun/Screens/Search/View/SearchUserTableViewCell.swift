//
//  SearchUserTableViewCell.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import UIKit
import SDWebImage

class SearchUserTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    func configure(with viewModel: SearchUserCellViewModelProtocol) {
		avatarView.sd_setImage(with: viewModel.avatarUrl, placeholderImage: viewModel.placeholder)
        nameLabel.text = viewModel.name
        typeLabel.text = viewModel.type
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.avatarView.image = nil
        nameLabel.text = "<Name>"
        typeLabel.text = "<Type>"
    }

}
