//
//  Repository2.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    
    var name: String { get }
    var language: String? { get }
    var updatedAt: Date { get }
    var stars: Int { get }
    
}

struct Repository: RepositoryProtocol, Codable {

    // MARK: - Internal Properties -
    
    let name: String
    let language: String?
    let updatedAt: Date
    let stars: Int
    
}

extension Repository {
    
    // MARK: - Internal Types -
    
    enum CodingKeys: String, CodingKey {
        case name
        case language
        case updatedAt = "updated_at"
        case stars = "stargazers_count"
    }
    
}
