//
//  UserCellViewModel.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import UIKit

protocol UserCellViewModelProtocol {
    
    var avatarURL: URL? { get }
    var avatarPlaceholder: UIImage { get }
    var name: String { get }
    var login: String { get }
    var createdAt: String { get }
    var location: String? { get }
    
}

struct UserCellViewModel: UserCellViewModelProtocol {
    
    let avatarURL: URL?
    let avatarPlaceholder: UIImage
    let name: String
    let login: String
    let createdAt: String
    let location: String?
    
    init(user: UserProtocol) {
        avatarURL = user.avatarUrl
        avatarPlaceholder = #imageLiteral(resourceName: "profile_placeholder")
        name = user.name ?? user.login
        login = user.login
        createdAt = user.createdAt?.formatted ?? " "
        location = user.location ?? " "
    }
}
