//
//  AppDelegate.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import UIKit
import CoreData

import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy private var dependencies: RouterDependencies = {
		return loadDependencies()
    }()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // TODO: Replace temp assembly
        dependencies.routerAssembly.assembleMainScreenAsRoot(with: dependencies)

        return true
    }

    func loadDependencies() -> RouterDependencies {
        let env                = Environment(.PROD)
        let errorHandler       = NetworkErrorHandler()

        let networkDispatcher  = NetworkDispatcher(environment: env, errorHandler: errorHandler)

        let searchService      = SearchService(with: networkDispatcher)
        let userService        = UserService(with: networkDispatcher)

        let routerAssembly	   = RouterAssembly()

        return RouterDependencies(
            routerAssembly: routerAssembly,
            searchService: searchService,
            userService: userService
        )
    }

}

