//
//  SearchService.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.

import CoreData
import RxSwift

protocol SearchServiceProtocol {

//    var users: [UserProtocol]? {get}
//    var userPages: [UserPageProtocol]? { get }
    
    func getUsersSignal(keyword: String?, page: Int?) -> Observable<UserPageProtocol>
//    func getUsersPageSignal(keyword: String?, page: UInt) -> Observable<UserPageProtocol>
}

class SearchService: SearchServiceProtocol {
	private var dispatcher: NetworkDispatcherProtocol

    init(with dispatcher: NetworkDispatcherProtocol) {
        self.dispatcher = dispatcher
    }
    
    //MARK: - Users
//    var users: [UserProtocol]?

    func getUsersSignal(keyword: String?, page: Int?) -> Observable<UserPageProtocol> {
        let signal = Observable<UserPageProtocol>.create() { observer -> Disposable in
            if let keyword = keyword, !keyword.isEmpty {
                let query = ["page": String(describing: (page ?? 1)), "q": keyword]
                let searchUsersRequest = SearchUserRequestOpeation(query: query, urlParameter: nil)
                do {
                    try searchUsersRequest.execute(in: self.dispatcher) { result in
                        switch (result) {
                        case .success(let data):
                            observer.on(.next(data))
                            observer.onCompleted()
                        case .failure(let error):
                            observer.onError(error)
                        }
                    }
                } catch let error {
                    observer.onError(error)
                }
            } else {
                do {
                    let json = "{ \"total_count\" : 0, \"items\": [] }"
                    let page = try JSONDecoder().decode(UserPage.self, from: json.data(using: .utf8)!)
                    observer.on(.next(page))
                }
                catch {
                    
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
        return signal
    }
    
//    var userPages: [UserPageProtocol]?
//    func getUsersPageSignal(keyword: String?, page: UInt) -> Observable<[UserPageProtocol]> {
//        let signal = Observable<UserPageProtocol>.create() { [page, keyword] observer -> Disposable in
//            if let keyword = keyword, !keyword.isEmpty {
//                let query = ["page": String(describing: page), "q": keyword]
//                let searchUsersRequest = SearchUserRequestOpeation(query: query, urlParameter: nil)
//                do {
//                    try searchUsersRequest.execute(in: self.dispatcher) { result in
//                        switch (result) {
//                        case .success(let users):
//                            observer.on(.next(users))
//                            observer.onCompleted()
//                        case .failure(let error):
//                            observer.onError(error)
//                        }
//                    }
//                } catch let error {
//                    observer.onError(error)
//                }
//            } else {
//                observer.on(.next([User]()))
//                observer.onCompleted()
//            }
//            return Disposables.create()
//        }
//        return signal
//    }

}
