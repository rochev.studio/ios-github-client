//
//  SearchInteractor.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import CoreData
import RxSwift

protocol SearchInteractorInputProtocol: class {

    func configure(with output: SearchInteractorOutputProtocol)
    func startDataLoading(keyword: String?, page: Int?)
}

protocol SearchInteractorOutputProtocol: class {
    func didLoadPageData(with userPage: UserPageProtocol?)
    func didFailLoadData(with error: Error)

}

class SearchInteractor: SearchInteractorInputProtocol {

    weak var output: SearchInteractorOutputProtocol?
    private let searchService: SearchServiceProtocol
    private let bag = DisposeBag()

    init(with search: SearchServiceProtocol) {
        self.searchService = search
    }

    func configure(with output: SearchInteractorOutputProtocol) {
        self.output = output
    }

    func startDataLoading(keyword: String?, page: Int?) {
        print("PAGE_DOWNLOAD \(page!)")
        searchService.getUsersSignal(keyword: keyword, page: page)
            .subscribe(onNext: { data in
//                func didLoadData(with userPage: UsersPageProtocol?)
                var dataOut = data
                    dataOut.pageNumber = page
                    self.output?.didLoadPageData(with: dataOut)
                }, onError: { [weak self] in
                    self?.output?.didFailLoadData(with: $0)
            })
            .disposed(by: bag)
    }
}
