//
//  SearchUserCellViewModelProtocol.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import Foundation
import UIKit

protocol SearchUserCellViewModelProtocol {
    var name: String { get }
    var login: String { get }
    var type: String { get }
    var avatarUrl: URL? { get }
    var placeholder: UIImage { get }
}

class SearchUserCellViewModel: SearchUserCellViewModelProtocol {
    var name: String
    var login: String
    var type: String
    var avatarUrl: URL?
    var placeholder: UIImage
    
    init(with user: UserProtocol) {
        login = user.login
        name = user.name ?? user.login
        type = user.type ?? "<no type>"
        avatarUrl = user.avatarUrl
        placeholder = #imageLiteral(resourceName: "profile_placeholder")
    }

}
