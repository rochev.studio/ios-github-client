//
//  UserRepositoryViewModelProtocol.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

protocol UserRepositoryCellViewModelProtocol {
    
    var name: String { get }
    var language: String { get }
    var stars: String { get }
    var updatedAt: String { get }
    var isDetailed: Bool { get }
    
    mutating func setDetailed(_ value: Bool)
}

struct UserRepositoryViewModel: UserRepositoryCellViewModelProtocol {
    
    let name: String
    let language: String
    let stars: String
    let updatedAt: String
    private(set) var isDetailed: Bool = false
    
    init(repository: RepositoryProtocol) {
        name = repository.name
        language = repository.language ?? "--"
        stars = "\(repository.stars.abbreviated) Stars"
        updatedAt = repository.updatedAt.formatted
    }
    
    mutating func setDetailed(_ value: Bool) {
        isDetailed = value
    }
}
