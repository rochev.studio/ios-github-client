//
//  UserPresenter.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

protocol UserInputProtocol {
    
    func configure(with view: UserViewInputProtocol)
    
}

class UserPresenter: UserInputProtocol, UserViewOutputProtocol, UserInteractorOutputProtocol {
    
    weak var view: UserViewInputProtocol?
    let interactor: UserInteractorInputProtocol
    let router: UserRouterInputProtocol
    
    var userElement: UserCellViewModelProtocol
    var repositoryElements: [UserRepositoryCellViewModelProtocol] = []

    init(with interactor: UserInteractorInputProtocol, router: UserRouterInputProtocol) {
        self.interactor = interactor
        self.router = router
        
        self.userElement = UserCellViewModel(user: router.dependencies.userService.user!)

        interactor.configure(with: self)
    }

    func configure(with view: UserViewInputProtocol) {
        self.view = view
        view.config(with: self)
    }
    
    func viewIsReady() {
        interactor.loadUserRepositoriesAndDetails()
    }
    
    func repositoryCellDidTap(at index: Int) {
        repositoryElements[index].setDetailed(!repositoryElements[index].isDetailed)
        view?.reloadRepositoryCell(at: index)
    }
    
    func didLoadUserRepositories(_ repositories: [RepositoryProtocol], user: UserProtocol) {
        repositoryElements = repositories
            .sorted { $0.stars > $1.stars }
            .map { UserRepositoryViewModel(repository: $0) }
        userElement = UserCellViewModel(user: user)
        view?.refreshUI()
    }
    
    func didFailLoadData(with error: Error) {
        view?.showFailureRequestAlert(with: error.localizedDescription, actionHandler: nil, completion: nil)
    }
}
