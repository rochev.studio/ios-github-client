//
//  SearchAssembly.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import Foundation

protocol SearchAssemblyProtocol {

 	func createModule(with view: SearchViewInputProtocol, dependencies: RouterDependencies)
    
}

class SearchAssembly {

    func createModule(with view: SearchViewInputProtocol, dependencies: RouterDependencies) {
        let interactor = SearchInteractor(with: dependencies.searchService)
        let router = SearchRouter(with: dependencies, view: view)
		let presenter = SearchPresenter(with: interactor, router: router)
        presenter.configure(with: view)
    }

}
