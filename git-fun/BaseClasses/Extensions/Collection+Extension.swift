//
//  Collection+Extension.swift
//  git-fun
//
//  Created by Paul Vasilenko on 05.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

extension Collection where Element: Collection {
    func contains(index: Index, subIndex: Element.Index) -> Bool {
       indices.contains(index) && self[index].indices.contains(subIndex)
   }
}
