//
//  UserInteractor.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation
import RxSwift

protocol UserInteractorInputProtocol: class {

    func configure(with output: UserInteractorOutputProtocol)
    func loadUserRepositoriesAndDetails()

}

protocol UserInteractorOutputProtocol: class {

    func didLoadUserRepositories(_ repositories: [RepositoryProtocol], user: UserProtocol)
    func didFailLoadData(with error: Error)
}

class UserInteractor: UserInteractorInputProtocol {
    
    private weak var output: UserInteractorOutputProtocol?
    private let userService: UserServiceProtocol
    private let bag: DisposeBag = DisposeBag()
    
    init(userService: UserServiceProtocol) {
        self.userService = userService
    }

    func configure(with output: UserInteractorOutputProtocol) {
        self.output = output
    }

    func loadUserRepositoriesAndDetails() {
        userService.getDetailsSignal()?.subscribe(onNext: { [unowned self] user in
            self.userService.getRepositoriesSignal()?.subscribe(onNext: { [unowned self] repositories in
                self.output?.didLoadUserRepositories(repositories, user: user)
            }, onError: { [unowned self] error in
                self.output?.didFailLoadData(with: error)
            }).disposed(by: bag)
        }, onError: { [unowned self] error in
            self.output?.didFailLoadData(with: error)
        }).disposed(by: bag)
    }
    
}
