//
//  Int+Extension.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

extension Int {
    
    var abbreviated: String {
        let signPart = self < 0 ? "- " : ""
        var value = self
        var index = 0
        let suffixArray = ["", "K", "M", "B", "T"]
        
        while value / 1000 >= 1 {
            value /= 1000
            index += 1
        }
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.decimalSeparator = "."
        formatter.minimumIntegerDigits = 1
        formatter.roundingMode = .down
        
        let valueWith1Digit = formatter.string(from: NSNumber(value: value)) ?? ""
        let suffixIndex = Swift.min(index, suffixArray.count - 1)
        let suffix = suffixArray[suffixIndex]
        
        return signPart + valueWith1Digit + suffix
    }
}
