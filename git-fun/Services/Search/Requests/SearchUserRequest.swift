//
//  SearchUserRequest.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.

//

import Foundation

class SearchUserRequestOpeation: RequestOperationProtocol {

    init(query parameters:[String: String]?, urlParameter: String?) {
        self.inputParameters = parameters
    }
    
    var inputParameters: [String: String]?
    var urlParameter: String?
    
    typealias ResponseModel = UserRequestResponseModel

    var request: RequestProtocol {
        let request = SearchUserRequest(query: self.inputParameters, urlParameter: self.urlParameter)
        return request
    }

    struct UserRequestResponseModel: ServerResponseModelProtocol {
        /**
         typealias Output = [UserDetails]

         enum CodingKeys: String, CodingKey {
             case data = "items"
         }

         var data: [UserDetails]
         */
        
        typealias Output = UserPage
        
        var data: UserPage
        
        //TODO: output user and total
        

        enum CodingKeys: String, CodingKey {
            case items = "items"
            case totalCount = "total_count"
        }
        
        init(from decoder: Decoder) throws {
            let map = try decoder.container(keyedBy: UserRequestResponseModel.CodingKeys.self)
            data = try UserPage(from: decoder)
            items = try map.decodeIfPresent([User].self, forKey: .items)
            totalCount = try map.decodeIfPresent(Int.self, forKey: .totalCount)
        }

        var items: [User]?
        var totalCount: Int?
    }

}

class SearchUserRequest: RequestProtocol {
    required init(query parameters: [String: String]?, urlParameter: String?) {
        self.urlParameter = urlParameter
        self.externalParameters = parameters
    }
    
    var path: String {
        return "search/users"
    }

    var dataType: DataType {
        return .JSON
    }

    var method: HTTPMethodType {
        return .get
    }

    var staticParameters: [String : String]? {
        return ["per_page" : "20"]
    }

    var externalParameters: [String : String]?
    var urlParameter: String?
    
    
}
