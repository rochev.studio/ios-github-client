//
//  UserRouter.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

protocol UserRouterInputProtocol {

    var dependencies: RouterDependencies { get }
    var view: UserViewInputProtocol { get }
    
}

class UserRouter: UserRouterInputProtocol {

    let dependencies: RouterDependencies
    let view: UserViewInputProtocol

    init(with dependencies: RouterDependencies, view: UserViewInputProtocol) {
        self.dependencies = dependencies
        self.view = view
    }
}
