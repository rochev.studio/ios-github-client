//
//  SearchRouter.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import UIKit

protocol SearchRouterInputProtocol {

    func showDetails(for model:UserProtocol)

}

class SearchRouter: SearchRouterInputProtocol {
    
    enum SearchRoutes: String {
        case userDetailsViewController = "UserDetailsViewController"
    }

    var dependencies: RouterDependencies
    let view: SearchViewInputProtocol

    init(with dependencies: RouterDependencies, view: SearchViewInputProtocol) {
		self.dependencies = dependencies
        self.view = view
    }
    
    func showDetails(for model: UserProtocol) {
        dependencies.userService.user = model
        dependencies.routerAssembly.pushUserScreen(on: view.navigationController, with: dependencies)
    }
}
