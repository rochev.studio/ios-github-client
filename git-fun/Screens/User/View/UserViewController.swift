//
//  UserViewController.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import UIKit

protocol UserViewInputProtocol: class, ShowActivityController, ShowAlertController {

    var navigationController: UINavigationController? { get }

    func config(with output: UserViewOutputProtocol)
    func refreshUI()
    func reloadRepositoryCell(at index: Int)
}

protocol UserViewOutputProtocol: class {
    
    var userElement: UserCellViewModelProtocol { get }
    var repositoryElements: [UserRepositoryCellViewModelProtocol] { get }
    
    func viewIsReady()
    func repositoryCellDidTap(at index: Int)
}

class UserViewController: BaseViewController, UserViewInputProtocol {
    
    enum CellIdentifiers {
        static let user: String = "UserCell"
        static let repository: String = "UserRepositoryCell"
    }

    // MARK: - Private properties -

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    private(set) var output: UserViewOutputProtocol!

    // MARK: - Configuration -

    func config(with output: UserViewOutputProtocol) {
        self.output = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicatorView.setIsHidden(false)
        navigationItem.title = output.userElement.login + "'s repos"
        output.viewIsReady()
    }
    
    func refreshUI() {
        if output.repositoryElements.isEmpty {
            tableView.reloadData()
        } else {
            tableView.reloadSections(IndexSet(arrayLiteral: 0, 1), with: .automatic)
        }
        
        activityIndicatorView.setIsHidden(true)
    }
    
    func reloadRepositoryCell(at index: Int) {
        let indexPath = IndexPath(row: index, section: 1)
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
}

extension UserViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : output.repositoryElements.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.user) as! UserCell
            cell.configure(with: output.userElement)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.repository) as! UserRepositoryCell
            cell.configure(with: output.repositoryElements[indexPath.row])
            return cell
        }
    }

}

extension UserViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section == 1 else {
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        output.repositoryCellDidTap(at: indexPath.row)
    }
    
}
