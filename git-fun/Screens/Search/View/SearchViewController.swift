//
//  SearchViewController.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import UIKit

protocol SearchViewInputProtocol: class, ShowActivityController, ShowAlertController {

    var navigationController: UINavigationController? { get }

    func config(with output: SearchViewOutputProtocol)
    func refreshUI()

}

protocol SearchViewOutputProtocol: class {
    
    var elementPages: [[SearchUserCellViewModelProtocol]] { get }
    
    func viewIsReady()
    func refreshData(keyword: String?, page: Int?)
    func viewDidPressDetailsButton(with index: Int, section: Int)
    func canLoadSectionAfter(_ section: Int) -> Bool
}

class SearchViewController: BaseViewController, SearchViewInputProtocol {

    enum CellIdentifiers: String {
        case repository = "SearchUserTableViewCell"
    }

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!

    // MARK: - Private properties -

    private(set) var output: SearchViewOutputProtocol!

    // MARK: - Configuration -

    func config(with output: SearchViewOutputProtocol) {
        self.output = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Users"
        searchBar.becomeFirstResponder()
        output.viewIsReady()
    }

    @objc func refreshView() {
        searchBar.text = nil
        output.refreshData(keyword: nil, page: 1)
    }

    func refreshUI() {
        tableView.reloadData()
    }

}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        showHUD()
        output.refreshData(keyword: searchBar.text, page: 1)
        searchBar.resignFirstResponder()
    }
}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        //TODO: Start loading if page not loaded
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.elementPages.count > section ? output.elementPages[section].count : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output.elementPages.count
    }
                   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.repository.rawValue) as! SearchUserTableViewCell
        cell.configure(with: output.elementPages[indexPath.section][indexPath.row])
        
        if indexPath.row == output.elementPages[indexPath.section].count - 1 { // last cell
            if output.canLoadSectionAfter(indexPath.section) { // more items to fetch //TODO: check in Presenter
                output.refreshData(keyword: searchBar.text, page: indexPath.section + 2) // increment `fromIndex` by 20 before server call
            }
        }
        
        return cell
    }

}

extension SearchViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.viewDidPressDetailsButton(with: indexPath.row, section: indexPath.section)
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
