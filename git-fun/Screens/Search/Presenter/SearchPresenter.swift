//
//  SearchPresenter.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

protocol SearchModuleInputProtocol {
	
}

class SearchPresenter: SearchModuleInputProtocol, SearchViewOutputProtocol, SearchInteractorOutputProtocol {

    weak var view: SearchViewInputProtocol?
    let interactor: SearchInteractorInputProtocol
    let router: SearchRouterInputProtocol
    var elementPages: [[SearchUserCellViewModelProtocol]] = [[SearchUserCellViewModelProtocol]]()
    var rawPageData: [UserPageProtocol]? = [UserPageProtocol]()
    private var processingPage: Int = 0
    private var previousKeyword: String?

    init(with interactor: SearchInteractorInputProtocol, router: SearchRouterInputProtocol) {
        self.interactor = interactor
        self.router = router

        interactor.configure(with: self)
    }

    func configure(with view: SearchViewInputProtocol) {
        self.view = view
        view.config(with: self)
    }

    func viewIsReady() {
    }
    
    ///clear state on keyword change
    private func clearState() {
        processingPage = 0
        rawPageData = [UserPageProtocol]()
        elementPages = [[SearchUserCellViewModelProtocol]]()
    }
    
    //TODO: проверка на уже загруженность страницы?
    func refreshData(keyword: String?, page: Int?) {
        if let prev = previousKeyword, prev != keyword {
            clearState()
        } else if previousKeyword == nil {
            previousKeyword = keyword
        }
        
        guard let page = page, elementPages.count - 1 < page, page > processingPage else {
            return
        }
        processingPage = page
        print("PAGE_DOWNLOAD \(page)")
        interactor.startDataLoading(keyword: keyword, page: page)
    }

    func didFailLoadData(with error: Error) {
        processingPage -= 1
        self.view?.showFailureRequestAlert(with: error.localizedDescription, actionHandler: nil, completion: nil)
    }
    
    func didLoadPageData(with userPage: UserPageProtocol?) {
        print("userPage: \(String(describing: userPage))")
        guard let page = userPage,
              let pageNumber = page.pageNumber,
              let items = page.items else {
            return
        }
        
        let section = pageNumber - 1
        
        if let raw = rawPageData, raw.indices.contains(section) {
            rawPageData?[section] = page
        } else {
            rawPageData?.append(page)
        }
        
        let elements = items.map { SearchUserCellViewModel(with: $0) }
        if elements.count > 0 {
            if elementPages.count == 0 || elementPages.count <= pageNumber {
                elementPages.append(elements)
            } else {
                elementPages[pageNumber - 1] = elements
            }
        }
            
        view?.hideHUD()
        view?.refreshUI()
    }
    
    func canLoadSectionAfter(_ section: Int) -> Bool {
        //section not overlaps last page section+1 <= lastPage
        //0 - true
        //48 - true 50 > 48 + 1
        //49 - false 50 > 49 + 1
        let can: Bool =  (rawPageData?.first?.lastPage ?? 0) > section + 1
        return can
    }

    func viewDidPressDetailsButton(with index: Int, section: Int) {
        guard let page = rawPageData?[section],
              let user = page.items?[index] else {
            fatalError()
        }
        
		router.showDetails(for: user)
    }
}
