//
//  User.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import Foundation
import CoreData

protocol UserProtocol {
    
    var avatarUrl: URL? { get }
    var identifier: Int64 { get }
    var name: String? { get }
    var type: String? { get }
    var login: String { get }
    var createdAt: Date? { get }
    var location: String? { get }
    
}

struct User: UserProtocol, Codable {
    
    let avatarUrl: URL?
    let name: String?
    let identifier: Int64
    let type: String?
    let login: String
    let createdAt: Date? = nil
    let location: String? = nil

    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case name
        case identifier = "id"
        case type
        case login
        case createdAt = "created_at"
        case location
    }
    
}

struct UserDetails: UserProtocol, Codable {
    
    let avatarUrl: URL?
    let name: String?
    let identifier: Int64
    let type: String? = nil
    let login: String
    let createdAt: Date?
    let location: String?

    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case name
        case identifier = "id"
        case type
        case login
        case createdAt = "created_at"
        case location
    }
}
