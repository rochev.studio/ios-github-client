//
//  UserAssembly.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

class UserAssembly {

    func createModule(with view: UserViewInputProtocol, dependencies: RouterDependencies) {
        let interactor = UserInteractor(userService: dependencies.userService)
        let router = UserRouter(with: dependencies, view: view)
        let presenter = UserPresenter(with: interactor, router: router)
        presenter.configure(with: view)
    }

}
