//
//  UserTableViewCell.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import UIKit
import SDWebImage

class UserCell: UITableViewCell {
    
    // MARK: - Private Properties -
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var loginLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var createdAtLabel: UILabel!
    
    // MARK: - Internal Methods -
    
    func configure(with viewModel: UserCellViewModelProtocol) {
        avatarImageView.sd_setImage(with: viewModel.avatarURL, placeholderImage: viewModel.avatarPlaceholder)
        nameLabel.text = viewModel.name
        loginLabel.text = viewModel.login
        locationLabel.text = viewModel.location
        createdAtLabel.text = viewModel.createdAt
    }

}
