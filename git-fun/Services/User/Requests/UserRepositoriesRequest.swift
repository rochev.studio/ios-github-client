//
//  SearchRequest.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import Foundation

class UserRepositoriesRequestOperation: RequestOperationProtocol {

    typealias ResponseModel = UserRepositoriesRequestResponseModel
    
    let inputParameters: [String : String]? = nil
    let urlParameter: String? = nil
    
    var request: RequestProtocol {
        return UserRepositoriesRequest(userLogin: userLogin)
    }

    struct UserRepositoriesRequestResponseModel: ServerResponseModelProtocol {

        typealias Output = [Repository]

        enum CodingKeys: String, CodingKey {
            case data = "items"
        }

        var data: Output

    }
    
    private let userLogin: String
    
    init(userLogin: String) {
        self.userLogin = userLogin
    }
    
}

class UserRepositoriesRequest: RequestProtocol {
    
    let urlParameter: String? = nil
    let externalParameters: [String : String]? = nil
    let staticParameters: [String : String]? = nil
    let parameters: [String : String]? = nil
    
    let dataType: DataType = .JSON
    let method: HTTPMethodType = .get
    
    var path: String {
        return "users/\(userLogin)/repos"
    }
    
    private let userLogin: String
    
    init(userLogin: String) {
        self.userLogin = userLogin
    }

}
