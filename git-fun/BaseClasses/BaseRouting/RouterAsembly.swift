//
//  RouterAsembly.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.
//

import UIKit

struct RouterDependencies {
    let routerAssembly: RouterAssemblyProtocol
    let searchService: SearchServiceProtocol
    var userService: UserServiceProtocol
}

protocol RouterAssemblyProtocol {
    
    func assembleMainScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies)
    func assembleMainScreenAsRoot(with dependencies: RouterDependencies)
    func pushUserScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies)
}

class RouterAssembly: NSObject, RouterAssemblyProtocol {

    func assembleMainScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let module = SearchAssembly()
        module.createModule(with: vc, dependencies: dependencies)
        navigation?.setViewControllers([vc], animated: false)
    }

    func assembleMainScreenAsRoot(with dependencies: RouterDependencies) {
        let rootNavController = UIApplication.shared.windows.first?.rootViewController as! UINavigationController
        assembleMainScreen(on: rootNavController, with: dependencies)
    }
    
    func pushUserScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserViewController") as! UserViewController
        let module = UserAssembly()
        module.createModule(with: vc, dependencies: dependencies)
        navigation?.pushViewController(vc, animated: true)
    }
    
}
