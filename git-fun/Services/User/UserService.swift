//
//  SearchService.swift
//  git-fun
//
//  Created by Paul Vasilenko on 25.09.2020.

import CoreData
import RxSwift

protocol UserServiceProtocol {

    var user: UserProtocol? { get set }
    
    func getRepositoriesSignal() -> Observable<[RepositoryProtocol]>?
    func getDetailsSignal() -> Observable<UserProtocol>?

}

class UserService: UserServiceProtocol {
    
    var user: UserProtocol?

    private let dispatcher: NetworkDispatcherProtocol

    init(with dispatcher: NetworkDispatcherProtocol) {
        self.dispatcher = dispatcher
    }

    func getRepositoriesSignal() -> Observable<[RepositoryProtocol]>? {
        guard let userLogin = user?.login else {
            return nil
        }
        
        return Observable<[RepositoryProtocol]>.create() { observer in
            let request = UserRepositoriesRequestOperation(userLogin: userLogin)
            do {
                try request.execute(in: self.dispatcher) { result in
                    switch (result) {
                    case .success(let repos):
                        observer.onNext(repos)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
            } catch let error {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }

    func getDetailsSignal() -> Observable<UserProtocol>? {
        guard let userLogin = user?.login else {
            return nil
        }
        
        return Observable<UserProtocol>.create() { observer in
            let request = UserDetailsRequestOperation(userLogin: userLogin)
            do {
                try request.execute(in: self.dispatcher) { result in
                    switch (result) {
                    case .success(let details):
                        observer.onNext(details[0])
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
            } catch let error {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }

}
