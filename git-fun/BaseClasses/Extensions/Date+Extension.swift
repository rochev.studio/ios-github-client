//
//  Date+Extension.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import Foundation

extension Date {
    
    var formatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.string(from: self)
    }
}
