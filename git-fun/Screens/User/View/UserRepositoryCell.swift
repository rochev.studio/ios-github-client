//
//  UserRepositoryCell.swift
//  git-fun
//
//  Created by Pavel Puzyrev on 01.10.2020.
//  Copyright © 2020 rochev.studio. All rights reserved.
//

import UIKit

class UserRepositoryCell: UITableViewCell {

    // MARK: - Private Properties -
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var starsLabel: UILabel!
    @IBOutlet private weak var updatedAtLabel: UILabel!
    @IBOutlet private weak var detailsContaner: UIView!
    
    // MARK: - Internal Methods -
    
    func configure(with viewModel: UserRepositoryCellViewModelProtocol) {
        nameLabel.text = viewModel.name
        languageLabel.text = viewModel.language
        starsLabel.text = viewModel.stars
        updatedAtLabel.text = viewModel.updatedAt
        detailsContaner.isHidden = !viewModel.isDetailed
        accessoryType = detailsContaner.isHidden ? .detailButton : .none
    }

}
